//
//  wholesome_recipe_grabberTests.m
//  wholesome_recipe_grabberTests
//
//  Created by shdwprince on 11/18/15.
//  Copyright © 2015 shdwprince. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "WSRecipeParser.h"

@interface wholesome_recipe_ingredientParserTests : XCTestCase
@property WSRecipeParser *parser;
@end

@implementation wholesome_recipe_ingredientParserTests

- (void)setUp {
    [super setUp];

    self.parser = [WSRecipeParser new];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)ingredientParserTestFor:(NSArray<NSString *> *) testStrings
                        results:(NSArray<NSObject *> *) testResults {
    for (int i = 0, results_idx = 0; i < testStrings.count; i++) {
        NSArray<WSIngredient *> *ingredients = [self.parser parseIngredients:testStrings[i]];
        for (int n = 0; n < ingredients.count; n++, results_idx++) {
            NSObject *expectedResult = testResults[results_idx];
            WSIngredient *o = ingredients[n];
            NSObject *result;
            if (o.strFallback) {
                result = o.strFallback;
            } else {
                result = [NSMutableArray new];
                [(NSMutableArray * ) result addObjectsFromArray:@[o.strName,
                                                                  o.strNote ? o.strNote : [NSNull null]]];
                for (WSIngredientAmount *amount in o.arrayAmounts) {
                    [(NSMutableArray *) result addObjectsFromArray:@[amount.nAmount ? amount.nAmount : [NSNull null],
                                                                     amount.strUnit ? amount.strUnit : [NSNull null],
                                                                     amount.strNote ? amount.strNote : [NSNull null]]];
                }
            }

            XCTAssertEqualObjects(result, expectedResult);
        }
    }
}

- (void)testIngredientParserStandart {
    NSArray<NSString *> *testStrings = @[@"1 cup (160 g) coconut oil, scoopable (not liquid)* (or, use this crust instead)",
                                         @"1/4 cup (55 g) dark or light muscovado sugar (or sub organic brown sugar or coconut sugar)",
                                         @"¼ cup oat flour (finely ground from rolled oats)",
                                         @"6Tbsp vegetable/ olive/ coconut",
                                         @"½ tsp ground cinnamon (optional)",
                                         @"100 kg fried something",
                                         @"1¼ cup dark chocolate chips",
                                         @"10kg (350o) fresh meat",
                                         @"10 sweet potatoes",
                                         ];
    NSArray<NSArray<NSString *> *> *testResults = @[
                                                    @[@"coconut oil, scoopable (not liquid)*", @"or, use this crust instead",
                                                      @1, @"cup", @"160 g"],
                                                    @[@"dark or light muscovado sugar",
                                                      @"or sub organic brown sugar or coconut sugar",
                                                      @0.25, @"cup", @"55 g", ],
                                                    @[@"oat flour",
                                                      @"finely ground from rolled oats",
                                                      @0.25, @"cup", [NSNull null]],
                                                    @[@"vegetable/ olive/ coconut", [NSNull null],
                                                      @6, @"Tbsp", [NSNull null]],
                                                    @[@"ground cinnamon", @"optional",
                                                      @0.5, @"tsp", [NSNull null]],
                                                    @[@"fried something", [NSNull null],
                                                      @100, @"kg", [NSNull null]],
                                                    @[@"dark chocolate chips", [NSNull null],
                                                      @1.25, @"cup", [NSNull null]],
                                                    @[@"fresh meat", [NSNull null],
                                                      @10, @"kg", @"350o",],
                                                    @[@"sweet potatoes", [NSNull null],
                                                      @10, [NSNull null], [NSNull null],],
                                                    ];

    [self ingredientParserTestFor:testStrings results:testResults];
}

- (void) testIngredientParserUnitPluralization {
    [self ingredientParserTestFor:@[@"2 cups spelt flour",
                                    @"2 teaspoones spelt flour",
                                    ]
                          results:@[@[@"spelt flour", [NSNull null],
                                      @2, @"cup", [NSNull null]],
                                    @[@"spelt flour", [NSNull null],
                                      @2, @"teaspoon", [NSNull null]],
                                    ]];
}

- (void)testIngredientParserMultipleAmounts {
    [self ingredientParserTestFor:@[@"¾ cup + 1 tbsp spelt flour",
                                    @"1 cup (125 g) + 1/2 teaspoon i_name",
                                    ]
                          results:@[@[@"spelt flour", [NSNull null],
                                      @0.75, @"cup", [NSNull null],
                                      @1, @"tbsp", [NSNull null]],
                                    @[@"i_name", [NSNull null],
                                      @1, @"cup", @"125 g",
                                      @0.5, @"teaspoon", [NSNull null]],
                                    ]];
}

- (void)testIngredientParserMultipleIngredients {
    [self ingredientParserTestFor:@[@"2 small eggs + 1 large egg",
                                    @"2 tbsp (10g) freshly ground flax seeds - i use rice and cheese (clan) + 5 tbsp water (foobar)"]
                          results:@[@[@"small eggs", [NSNull null],
                                      @2, [NSNull null], [NSNull null]],
                                    @[@"large egg", [NSNull null],
                                      @1, [NSNull null], [NSNull null]],
                                    @[@"freshly ground flax seeds - i use rice and cheese", @"clan",
                                      @2, @"tbsp", @"10g"],
                                    @[@"water", @"foobar",
                                      @5, @"tbsp", [NSNull null]],
                                    ]];
}

- (void)testIngredientsParserUnitless {
    [self ingredientParserTestFor:@[@"8 chicken bones (with meat)",
                                    @"1 large egg",
                                    ]
                          results:@[@[@"chicken bones", @"with meat",
                                      @8, [NSNull null], [NSNull null],],
                                    @[@"large egg", [NSNull null],
                                      @1, [NSNull null], [NSNull null], ],
                                    ]];
}

- (void)testIngredientParserProblematic {
    [self ingredientParserTestFor:@[@"2 Naan flatbreads - I used the Stonefire brand.",
                                    ]
                          results:@[@[@"Naan flatbreads - I used the Stonefire brand.", [NSNull null],
                                      @2, [NSNull null], [NSNull null],],
                                    ]];
}

- (void)testIngredientParserFailing {
    NSArray<NSString *> *testStrings = @[@"1. foobar",
                                         @"№ foobar",
                                         @"foobar",
                                         @"Pinch of red pepper flakes", ];

    [self ingredientParserTestFor:testStrings results:@[@[@"1. foobar", [NSNull null], @1, [NSNull null], [NSNull null]],
                                                        @[@"№ foobar", [NSNull null], @1, [NSNull null], [NSNull null]],
                                                        @[@"foobar", [NSNull null], @1, [NSNull null], [NSNull null]],
                                                        @[@"Pinch of red pepper flakes", [NSNull null], @1, [NSNull null], [NSNull null]],
                                                        ]];
}

- (void)testIngredientAmountParser {
    NSArray<NSString *> *testStrings = @[@"1", @"0.5+1/2", @"0.5 + 1 / 2", @"0.25 1 / 2 + ¼", @"0.75¼", @" 0.25 3/4 " ];

    for (NSString *str in testStrings) {
        XCTAssertEqualObjects([self.parser parseAmount:str], @1);
    }
}

- (void) testIngredientParserAmountless {
    [self ingredientParserTestFor:@[@"chicken egg", ]
                          results:@[@[@"chicken egg", [NSNull null], @1, [NSNull null], [NSNull null]]
                                    ]];
}

- (void)testPerformanceExample {
    NSString *str = @"1/4 cup (55 g) and 1 teaspoon (150gr) dark or light muscovado sugar (or sub organic brown sugar or coconut sugar)";
    [self measureBlock:^{
        for (int i = 0; i < 100; i++) {
            [self.parser parseIngredients:str];
        }
    }];
}

@end
