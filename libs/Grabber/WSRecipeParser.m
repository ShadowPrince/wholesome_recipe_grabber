//
//  WSRecipeParser.m
//  wholesome_recipe_grabber
//
//  Created by shdwprince on 11/19/15.
//  Copyright © 2015 shdwprince. All rights reserved.
//

#import "WSRecipeParser.h"

static NSString *const WSRecipeParser_identationString = @"* •";
static NSString *const WSRecipeParser_unitsString = @"kg|mg|g|lb|pound|tsp|tbs|tbsp|teaspoon|spoon|sp|tablespoon|t|can|clove|packet|bunch|cup|cup sliced|cup, slices|fl oz|medium|oz|pill|raw|scoop|slice|spear|whole|c|oz|pint";
static NSString *const WSRecipeParser_partsSplitRegex = @"( ?\\+ ?)";

@interface WSRecipeParser ()
@property WSIngredientDatabase *ingredientDb;
@end@implementation WSRecipeParser

- (instancetype) init {
    self = [super init];
    self.ingredientDb = [WSIngredientDatabase db];
    return self;
}

#pragma mark - parsing

- (WSRecipe *) parseRecipe:(NSDictionary *) dict {
    WSRecipe *recipe = [WSRecipe new];

    recipe.strName = dict[@"name"] == [NSNull null] ? nil : [self stringify:dict[@"name"]];
    recipe.strYield = dict[@"recipeYield"] == [NSNull null] ? nil : [self stringify:dict[@"recipeYield"]];
    recipe.strDescription = dict[@"description"] == [NSNull null] ? nil : [self stringify:dict[@"description"]];
    if (dict[@"author"] != [NSNull null]) {
        NSObject *author = dict[@"author"];

        if ([author isKindOfClass:[NSString class]]) {
            recipe.strAuthor = (NSString *) author;
        } else if ([author isKindOfClass:[NSArray class]]) {
            recipe.strAuthor = [(NSArray *) author componentsJoinedByString:@", "];
        }
    }

    NSMutableArray *parsedIngredients = [NSMutableArray new];

    NSArray *ingredients;
    if (dict[@"ingredients"] != [NSNull null] && dict[@"ingredients"] != nil) {
        ingredients = dict[@"ingredients"];
    } else {
        ingredients = dict[@"recipeIngredient"];
    }
    
    if (ingredients) {
        for (NSString *ingredient in ingredients) {
            NSString *trimmedIngredient = [ingredient stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

            if (trimmedIngredient.length) {
                [parsedIngredients addObjectsFromArray:[self parseIngredients:trimmedIngredient]];
            }
        }
    }

    //@TODO: more appropriate place?
    for (WSIngredient *ingr in parsedIngredients) {
        [self.ingredientDb lookupAndPopulateIngredient:ingr];
    }

    recipe.arrayIngredients = (NSArray *) parsedIngredients;

    return recipe;
}

- (NSArray<WSIngredient *> *) parseIngredients:(NSString *) str {
    NSError *regexError;
    NSMutableString *string = str.mutableCopy;
    WSIngredient *ingredient = [WSIngredient new];

    for (NSString *identationPart in [WSRecipeParser_identationString componentsSeparatedByString:@" "]) {
        str = [str stringByReplacingOccurrencesOfString:identationPart withString:@""];
    }

    // a digit (w/ unicode chars), or space, or plus, or slash = number;
    // followed by space, followed by word == unit;
    // followed optionally by space and sequence in parentheses = note;
    NSString *amountRegexString = [NSString stringWithFormat:
                                   @"([\\d⅓¼½¾ \\+\\/]+) ?(((%@)e?s?\\.?){1})?( \\((.*?)\\){1}?)? ",
                                   WSRecipeParser_unitsString];
    NSRegularExpression *othersRegex = [WSRegexHelperMatch compileRegex:[@"^(and|plus|\\+){1} " stringByAppendingString:amountRegexString]
                                                                options:0
                                                                  error:&regexError];

    NSMutableArray *resultAmounts = [NSMutableArray new];

    // first to check for firstRegex, skipping 1 result (full match string) from matches array
    // after check for matches of othersRegex (skipping 2 first matches - full match & and|+)
    // until there's no
    // after every iteration remove matched text from string
    int matches_offset = 1;
    WSRegexHelperMatches *matches = [WSRegexHelperMatch firstMatchOfRegex:[@"^" stringByAppendingString:amountRegexString]
                                                              withOptions:NSRegularExpressionCaseInsensitive
                                                                 inString:string
                                                                    error:&regexError];
    while (matches.count) {
        WSIngredientAmount *amount = [WSIngredientAmount new];
        NSString *amountString = matches[0+matches_offset].match;
        NSNumber *amountNumber = [WSRecipeParser parseAmount:amountString];

        if (amountNumber) {
            amount.strAmount = amountString;
            amount.nAmount = amountNumber;

            if (matches.count > 3+matches_offset)
                amount.strUnit = matches[3+matches_offset].match;

            if (matches.count > 4+matches_offset)
                amount.strNote = matches[5+matches_offset].match;
        } else {
            amount.strFallback = amountString;
        }

        [resultAmounts addObject:amount];

        [string deleteCharactersInRange:matches.firstObject.range];
        matches = [WSRegexHelperMatch firstMatchOfRegularExpression:othersRegex
                                                           inString:string];
        matches_offset = 2; // because othersRegex contain one additional group (and|+)
    }

    //@TODO: decide what to do with amountless
    if (regexError || resultAmounts.count == 0) {
        ingredient.strName = str;
        WSIngredientAmount *amount = [WSIngredientAmount new];
        amount.strAmount = @"1";
        amount.nAmount = @1;
        ingredient.arrayAmounts = @[amount, ];
        return @[ingredient];
    } else {
        NSMutableArray *parsedIngredients = [NSMutableArray new];
        // add original ingredient object reference, so other ingredients will be added after it
        [parsedIngredients addObject:ingredient];

        NSArray<NSArray<NSString *> *> *parts = [WSRegexHelperMatch matchSplitArrayBySplitting:string withRegex:WSRecipeParser_partsSplitRegex];
        // split rest of the string by +|and|plus and try to parse parts by ingredient parser
        NSMutableString *strName = [parts.firstObject[1] mutableCopy];
        for (NSArray<NSObject *> *part in [parts subarrayWithRange:NSMakeRange(1, parts.count - 1)]) {
            WSIngredient *parsedIngredient = [self parseIngredients:(NSString *) part[1]].firstObject;
            if (!parsedIngredient.strFallback) {
                // if parsed successfully, add it to result array
                [parsedIngredients addObject:parsedIngredient];
            } else {
                // otherwise - add original part and original separator to name string
                [strName appendFormat:@"%@%@", part[0] == [NSNull null] ? @"" : part[0], part[1]];
            }
        }

        // check for note at the end of the name
        NSString *strNote = nil;
        matches = [WSRegexHelperMatch firstMatchOfRegex:@" (\\([^\\)]*?\\))+$"
                                            withOptions:0
                                               inString:strName
                                                  error:&regexError];
        if (matches.count) {
            strNote = [matches[1].match substringWithRange:NSMakeRange(1, matches[1].match.length-2)];
            [strName deleteCharactersInRange:matches.firstObject.range];
        }


        ingredient.strName = strName;
        ingredient.strNote = strNote;
        ingredient.arrayAmounts = resultAmounts;

        return parsedIngredients;
    }
}

+ (NSNumber *) parseAmount:(NSString *) stringNumber {
    NSMutableDictionary *pairs = [NSMutableDictionary new];
    // unicode symbols
    [pairs addEntriesFromDictionary:@{@"½": @"+0.5",
                                      @"¼": @"+0.25",
                                      @"⅓": @"+0.33",
                                      @"¾": @"+0.75",
                                      }];
    // 1/3, 1/2, 1/5, etc
    for (int t = 1; t < 10; t++) {
        for (int d = 1; d < 10; d++) {
            NSString *result = [NSString stringWithFormat:@"+%f", (float) t/d];
            pairs[[NSString stringWithFormat:@"%d/%d", t, d]] = result;
            pairs[[NSString stringWithFormat:@"%d / %d", t, d]] = result;
        }
    }

    __block NSString *normalizedString = stringNumber.copy;
    [pairs enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        normalizedString = [normalizedString stringByReplacingOccurrencesOfString:key withString:obj];
    }];

    // remove redundant +
    normalizedString = [WSRegexHelperMatch stringByReplacingRegex:@"(\\s*\\+\\s*)+"
                                                      withOptions:0
                                                     withTemplate:@"+"
                                                         inString:normalizedString
                                                            error:nil];
    // remove plus at the start
    normalizedString = [WSRegexHelperMatch stringByReplacingRegex:@"^\\s*\\+*"
                                                      withOptions:0
                                                     withTemplate:@""
                                                         inString:normalizedString
                                                            error:nil];
    // and at the end
    normalizedString = [WSRegexHelperMatch stringByReplacingRegex:@"\\+*\\s*$"
                                                      withOptions:0
                                                     withTemplate:@""
                                                         inString:normalizedString
                                                            error:nil];

    double value = 0;
    BOOL parseSuccess = NO;
    for (NSString *part in [normalizedString componentsSeparatedByString:@"+"]) {
        if (part.doubleValue) {
            parseSuccess = YES;
            value += part.doubleValue;
        }
    }

    if (!parseSuccess) {
        return nil;
    } else {
        return [NSNumber numberWithDouble:value];
    }
}

#pragma mark - helper

- (NSString *) stringify:(NSObject *) object {
    if ([object isKindOfClass:[NSArray class]])
        object = [(NSArray *) object firstObject];
    else if ([object isKindOfClass:[NSDictionary class]])
        object = [(NSDictionary *) object allValues].firstObject;

    return object.description;
}

@end
