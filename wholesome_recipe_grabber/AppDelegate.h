//
//  AppDelegate.h
//  wholesome_recipe_grabber
//
//  Created by shdwprince on 11/18/15.
//  Copyright © 2015 shdwprince. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

