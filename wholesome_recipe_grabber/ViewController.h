//
//  ViewController.h
//  wholesome_recipe_grabber
//
//  Created by shdwprince on 11/18/15.
//  Copyright © 2015 shdwprince. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WSRecipe.h"
#import "WSRecipeGrabber.h"
#import "WSIngredientDatabase.h"

#import "ResultsViewController.h"

@interface ViewController : UIViewController<UIWebViewDelegate, UIGestureRecognizerDelegate>


@end

