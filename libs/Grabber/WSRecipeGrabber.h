//
//  RecipeGrabber.h
//  wholesome_recipe_grabber
//
//  Created by shdwprince on 11/18/15.
//  Copyright © 2015 shdwprince. All rights reserved.
//

#import <Foundation/Foundation.h>

#if TARGET_OS_IPHONE
    #import <UIKit/UIKit.h>
#elif TARGET_OS_MAC
    #import <AppKit/AppKit.h>
#endif

#import <WebKit/WebKit.h>
#import "WSRecipeParser.h"

/**
 * Universal interface to grabber source. Usually implemented by WebView of some sort.
 */
@interface WSRecipeGrabberSource <NSObject>
- (NSURL *) currentUrl;
// evaluate javascript code
- (NSString *) evaluateString:(NSString *) string;

@end

@interface WKUserScript (UserScriptFromPath)
- (instancetype) initWithResource:(NSString *) res ofType:(NSString *) type injectionTime:(WKUserScriptInjectionTime) time;
@end

/**
 * Main class for recipe grabbing.
 * Handles every javascript communication and injecting process.
 */
@interface WSRecipeGrabber : NSObject
// was code injected before
@property (readonly) BOOL isCodeInjected;

- (instancetype) initWithSource:(WSRecipeGrabberSource *) source;

// reset isCodeInjected
- (void) reset;
// inject JS code (should be called once)
- (void) injectCode;

// grab entire recipe as dictionary. values may be [NSNull null]
- (WSRecipe *) grabRecipe;
// grab image URL at absolute position
- (NSString *) grabImageURLAt:(CGPoint) point;
// everything above is pretty self-explanatory
- (NSString *) grabDomain;
- (NSString *) grabTitle;
- (NSString *) grabSitename;
- (NSURL *) grabURL;

@end
