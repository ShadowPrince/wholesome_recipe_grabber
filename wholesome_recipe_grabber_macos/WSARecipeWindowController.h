//
//  WSARecipeWindowController.h
//  wholesome_recipe_grabber
//
//  Created by shdwprince on 1/11/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import "AppDelegate.h"
#import "WSAWebController.h"
#import "WSARecipeAutocompletion.h"

@interface WSARecipeWindowController : NSWindowController<NSControlTextEditingDelegate, NSTextViewDelegate, NSWindowDelegate>
// Create controller, show window and return it
+ (WSARecipeWindowController *) presentedControllerWith:(WSAGrabResult *) result;

@end
