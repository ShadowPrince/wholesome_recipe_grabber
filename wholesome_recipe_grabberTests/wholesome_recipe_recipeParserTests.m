//
//  wholesome_recipe_recipeParserTests.m
//  wholesome_recipe_grabber
//
//  Created by shdwprince on 11/19/15.
//  Copyright © 2015 shdwprince. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "WSRecipeParser.h"

@interface wholesome_recipe_recipeParserTests : XCTestCase
@property WSRecipeParser *parser;
@end

@implementation wholesome_recipe_recipeParserTests

- (void)setUp {
    [super setUp];

    self.parser = [WSRecipeParser new];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void) recipeParserTestFor:(NSArray *) testStrings
                      expect:(NSArray *) testResults {
    for (int i = 0; i < testStrings.count; i++) {
        WSRecipe *r = [self.parser parseRecipe:testStrings[i]];

        NSArray *actualResult = @[r.strName ? r.strName : [NSNull null],
                                  r.strDescription ? r.strDescription : [NSNull null],
                                  r.strAuthor ? r.strAuthor : [NSNull null],
                                  r.strYield ? r.strYield : [NSNull null],
                                  [NSNumber numberWithInt:r.arrayIngredients.count]];
        XCTAssertEqualObjects(testResults[i], actualResult);
    }
}

- (void)testVaryingIngredients {
    [self recipeParserTestFor:@[@{@"name": @"foobar", @"description": @"barfoo", @"author": @"coconut", @"recipeYield": @"yield",
                                  @"ingredients": @[@"foo", @"bar", ], },
                                @{@"recipeIngredient": @[@"foo", @"bar"]},
                                  ]
                       expect:@[@[@"foobar", @"barfoo", @"coconut", @"yield", @2],
                                @[[NSNull null], [NSNull null], [NSNull null], [NSNull null], @2],
                                ]];
}

@end
