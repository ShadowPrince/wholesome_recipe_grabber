//
//  ResultsViewController.h
//  wholesome_recipe_grabber
//
//  Created by shdwprince on 11/18/15.
//  Copyright © 2015 shdwprince. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WSRecipe.h"

@interface ResultsViewController : UIViewController
@property NSDictionary *results;

@end
