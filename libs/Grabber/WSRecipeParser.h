//
//  WSRecipeParser.h
//  wholesome_recipe_grabber
//
//  Created by shdwprince on 11/19/15.
//  Copyright © 2015 shdwprince. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSRecipe.h"
#import "WSIngredientDatabase.h"

@interface WSRecipeParser : NSObject

- (WSRecipe *) parseRecipe:(NSDictionary *) dict;
- (NSArray<WSIngredient *> *) parseIngredients:(NSString *) str;
+ (NSNumber *) parseAmount:(NSString *) stringNumber;

@end
