//
//  WSAWebWindowController.h
//  wholesome_recipe_grabber
//
//  Created by shdwprince on 1/6/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>
#import "AppDelegate.h"

#import "WSAWebController.h"

@interface WSAWebWindowController : NSWindowController<WSAWebControllerDelegate, NSTextFieldDelegate>

@end
