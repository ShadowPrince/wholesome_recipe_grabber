//
//  ViewController.m
//  wholesome_recipe_grabber
//
//  Created by shdwprince on 11/18/15.
//  Copyright © 2015 shdwprince. All rights reserved.
//

#import "ViewController.h"

// Implementation of WSRecipeGrabberSource for UIWebView
@implementation UIWebView (WSRecipeGrabberSource)
- (NSURL *) currentUrl {
    return self.request.URL;
}

- (NSString *) evaluateString:(NSString *) string {
    return [self stringByEvaluatingJavaScriptFromString:string];
}

@end


@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *urlTextField;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property NSString *lastInjectUrl;
@property WSRecipeGrabber *grabber;

@end@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [WSIngredientDatabase db];
    // you init grabber once, and use [grabber reset] when changing page
    self.grabber = [[WSRecipeGrabber alloc] initWithSource:(WSRecipeGrabberSource *) self.webView];
    [self loadPageAtURL:@"https://foodgawker.com/"];
}


- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.webView.scrollView.contentInset = UIEdgeInsetsMake(58.f, 0, 0, 0);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - webview

- (void) webViewDidFinishLoad:(UIWebView *)webView {
    if (![self.lastInjectUrl isEqualToString:webView.request.URL.absoluteString]) {
        [self.grabber reset];
    }

    if (!self.grabber.isCodeInjected) {
        [self.grabber injectCode];
        self.lastInjectUrl = webView.request.URL.absoluteString;
    }
}

#pragma mark - actions

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"results"]) {
        [(ResultsViewController *) segue.destinationViewController setResults:sender];
    }

    [super prepareForSegue:segue sender:sender];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (IBAction)gestureAction:(UILongPressGestureRecognizer *)sender {
    if (sender.state != UIGestureRecognizerStateBegan) {
        return;
    }

    if (!self.grabber.isCodeInjected) {
        return;
    }

    CGPoint loc = [sender locationInView:self.webView];

    CGSize viewSize = [self.webView frame].size;
    CGSize windowSize = [self windowSize];
    CGFloat f = windowSize.width / viewSize.width;

    loc.x = loc.x * f;
    loc.y = loc.y * f;

    NSMutableDictionary *results = [NSMutableDictionary new];

    [results addEntriesFromDictionary:@{@"domain": [self.grabber grabDomain] ? [self.grabber grabDomain] : @"",
                                        @"sitename": [self.grabber grabSitename] ? [self.grabber grabSitename] : @"",
                                        @"url": [self.grabber grabURL],
                                        @"title": [self.grabber grabTitle] ? [self.grabber grabTitle] : @"",
                                        @"imageSrc": [self.grabber grabImageURLAt:loc] ? [self.grabber grabImageURLAt:loc] : @"", }];

    results[@"recipe"] = [self.grabber grabRecipe];
    NSLog(@"%@", results);
    [self performSegueWithIdentifier:@"results" sender:results];
}

- (IBAction)goButtonAction:(id)sender {
    [self loadPageAtURL:self.urlTextField.text];
}

#pragma mark - helper

- (CGSize)windowSize {
    CGSize size;
    size.width = [[self.webView stringByEvaluatingJavaScriptFromString:@"window.innerWidth"] integerValue];
    size.height = [[self.webView stringByEvaluatingJavaScriptFromString:@"window.innerHeight"] integerValue];
    return size;
}

- (void) loadPageAtURL:(NSString *) url {
    self.urlTextField.text = url;
    NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    [self.webView loadRequest:req];
    [self.grabber reset];
}

@end
