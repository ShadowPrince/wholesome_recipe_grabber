//
//  WSARecipeAutocompletion.m
//  wholesome_recipe_grabber
//
//  Created by shdwprince on 1/11/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import "WSARecipeAutocompletion.h"

@implementation NSNib (InstantiateAndGetView)
- (NSView *) instantiateViewWithOwner:(id) owner {
    NSArray *objects;
    [self instantiateWithOwner:owner topLevelObjects:&objects];

    for (id object in objects) {
        if ([object isKindOfClass:[NSView class]]) {
            return (NSView *) object;
        }
    }

    return nil;
}

@end

@implementation WSAView
@synthesize tag = _tag;

@end

@implementation WSARecipeAutocompletionItem
+ (WSARecipeAutocompletionItem *) itemWithIdentifier:(NSNumber *) identifier name:(NSString *) name type:(NSString *) type {
    WSARecipeAutocompletionItem *item = [WSARecipeAutocompletionItem new];
    item.identifier = identifier;
    item.name = name;
    item.type = type;
    item.string = [NSString stringWithFormat:@"#%@ %@, %@", item.identifier, item.type, item.name];

    return item;
}

+ (NSString *) formattedIngredient:(WSIngredient *) ingredient {
    if (ingredient.dbId) {
        return [NSString stringWithFormat:@"#%@ %@%@", ingredient.dbId, ingredient.dbType ? [ingredient.dbType stringByAppendingString:@", "] : @"", ingredient.dbName];
    } else {
        return @"";
    }
}

@end

@interface WSARecipeAutocompletionList ()
@property NSMutableArray *items;

@end @implementation WSARecipeAutocompletionList

+ (WSARecipeAutocompletionList *) listForString:(NSString *)searchString {
    WSARecipeAutocompletionList *instance = [WSARecipeAutocompletionList new];
    instance.items = [NSMutableArray new];

    // lookup DB for search term
    WSIngredientDatabaseLookupResult *lookup = [[WSIngredientDatabase db] lookupIngredientsFor:searchString];

    // to sort LookupResult we first must put in into array (since .results is a dict)
    NSMutableArray *weightIndex = [NSMutableArray new];
    for (NSData *uid in lookup.results) {
        [weightIndex addObject:@[uid, lookup.results[uid], ]];
    }

    [weightIndex sortUsingComparator:^NSComparisonResult(NSArray *obj1, NSArray *obj2) {
        NSNumber *first = obj1[1];
        NSNumber *second = obj2[1];
        return first.floatValue < second.floatValue;
    }];

    for (NSArray *indexElement in weightIndex) {
        MHSearchResultItem *item = lookup.resultItems[indexElement.firstObject];
        [instance.items addObject:[WSARecipeAutocompletionItem itemWithIdentifier:item.context[@"UsdaID"]
                                                                             name:item.context[@"Name"]
                                                                             type:item.context[@"Type"]]];
    }

    return instance;
}

- (NSArray<NSString *> *) stringArray {
    NSMutableArray *result = [NSMutableArray new];
    for (WSARecipeAutocompletionItem *item in self.items) {
        [result addObject:item.string];
    }

    return result;
}

- (WSARecipeAutocompletionItem *) itemForSuffixOf:(NSString *)string {
    for (WSARecipeAutocompletionItem *item in self.items) {
        if ([string hasSuffix:item.string]) {
            return item;
        }
    }

    return nil;
}

@end
