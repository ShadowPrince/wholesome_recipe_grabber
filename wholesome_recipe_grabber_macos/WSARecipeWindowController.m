//
//  WSARecipeWindowController.m
//  wholesome_recipe_grabber
//
//  Created by shdwprince on 1/11/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import "WSARecipeWindowController.h"

typedef enum : NSInteger {
    WSAIngredientViewDBAmount = -10,
    WSAIngredientViewUnit,
    WSAIngredientViewDBName,
    WSAIngredientViewFallback,
    WSAIngredientViewUIName,
    WSAIngredientViewUIAmount,
} WSAIngredientViewControls;

@interface WSARecipeWindowController ()
@property WSAGrabResult *result;
// Store last autocomple list here to find out DB's when user selects anything
@property WSARecipeAutocompletionList *autocompleteLast;

@property (weak) IBOutlet NSScrollView *ingredientsView;
@property (weak) IBOutlet NSTextField *recipeNameField;
@property (weak) IBOutlet NSTextField *recipeYieldField;
@property (weak) IBOutlet NSTextField *recipeImageUrlField;
@property (weak) IBOutlet NSImageView *recipeImageView;
@property (unsafe_unretained) IBOutlet NSTextView *recipeDescriptionField;


@end @implementation WSARecipeWindowController

+ (WSARecipeWindowController *) presentedControllerWith:(WSAGrabResult *) result {
    WSARecipeWindowController *instance = [[WSARecipeWindowController alloc] initWithWindowNibName:@"WSARecipeWindowController"];
    instance.result = result;

    [instance showWindow:nil];
    [instance populateIngredientsView];
    [instance populateMetadataViews];
    
    return instance;
}

- (void) windowWillClose:(NSNotification *)notification {
    [(AppDelegate *) [NSApp delegate] removeController:self];
}

- (NSArray<NSLayoutConstraint *> *) constraintsForViewAt:(NSUInteger) i of:(NSUInteger) count height:(NSNumber *) height view:(NSView *) view previous:(NSView *) previous {
    if (i == 0) {
        return [NSLayoutConstraint
                constraintsWithVisualFormat:@"V:|-0-[v(h)]"
                options:0
                metrics:@{@"h": height, }
                views:@{@"v": view, }];
    } else if (i == count - 1) {
        return [NSLayoutConstraint
                constraintsWithVisualFormat:@"V:[p]-0-[v(h)]-0-|"
                options:0
                metrics:@{@"h": height, }
                views:@{@"v": view, @"p": previous }];
        
    } else {
        return [NSLayoutConstraint
                constraintsWithVisualFormat:@"V:[p]-0-[v(h)]"
                options:0
                metrics:@{@"h": height, }
                views:@{@"v": view, @"p": previous }];
    }

}

- (void) populateIngredientsView {
    WSRecipe *recipe = (WSRecipe *) self.result[@"recipe"];

    // Since NSCollectionView is horribly broken right now we use a simple NSScrollView and a view from NSNib
    NSNumber *height = @70;
    NSView *previous = nil;
    NSNib *nib = [[NSNib alloc] initWithNibNamed:@"WSARecipeIngredientView" bundle:nil];

    int i = 0;
    for (WSIngredient *ingredient in recipe.arrayIngredients) {
        // superview's owner will be self, so we can use IB to quickly connect actions and stuff
        WSAView *view = (WSAView *) [nib instantiateViewWithOwner:self];
        // superview's tag used to store ingredient ID
        view.tag = i;

        NSTextField *amountField = [view viewWithTag:WSAIngredientViewDBAmount];
        NSTextField *unitField = [view viewWithTag:WSAIngredientViewUnit];
        NSTextField *ingredientField = [view viewWithTag:WSAIngredientViewDBName];
        NSTextField *fallbackField = [view viewWithTag:WSAIngredientViewFallback];
        NSTextField *ingredientUINameField = [view viewWithTag:WSAIngredientViewUIName];
        NSTextField *ingredientUIAmountField = [view viewWithTag:WSAIngredientViewUIAmount];

        WSIngredientAmount *am = ingredient.arrayAmounts.count > 0 ? ingredient.arrayAmounts[0] : nil;
        if (am) {
            amountField.floatValue = am.nAmount.floatValue;
            unitField.stringValue = am.strUnit ? am.strUnit : @"";

            ingredientUIAmountField.stringValue = am.strAmount;
        }

        // fill the DB ingredient name field with string formatted the way autocompletion formats it
        ingredientField.stringValue = [WSARecipeAutocompletionItem formattedIngredient:ingredient];
        fallbackField.stringValue = ingredient.inputString;
        
        ingredientUINameField.stringValue = ingredient.strName;

        view.translatesAutoresizingMaskIntoConstraints = NO;
        [self.ingredientsView.documentView addSubview:view];

        [self.ingredientsView addConstraints:[self constraintsForViewAt:i of:recipe.arrayIngredients.count height:height view:view previous:previous]];
        [self.ingredientsView addConstraints:[NSLayoutConstraint
                                              constraintsWithVisualFormat:@"|-0-[v]-0-|"
                                              options:0
                                              metrics:@{@"h": height, }
                                              views:@{@"v": view, }]];

        previous = view;
        i++;
    }

    // since, again, NSScrollView doesn't work well with the constraints we have to calculate contentSize manually
    [self.ingredientsView.documentView setFrame:NSMakeRect(0, 0, 400, 70 * i)];
    // scroll back to the beginning
    [self.ingredientsView.documentView scrollPoint:NSMakePoint(0, 70 * i)];
}

- (void) populateMetadataViews {
    self.recipeImageView.image = [[NSImage alloc] initWithContentsOfURL:[NSURL URLWithString:self.result[@"imageSrc"]]];
    self.recipeNameField.stringValue = self.result[@"title"] ? self.result[@"title"] : @"";
    
    WSRecipe *recipe = (WSRecipe *) self.result[@"recipe"];
    self.recipeYieldField.stringValue = recipe.strYield ? recipe.strYield : @"";
    self.recipeDescriptionField.string = recipe.strDescription ? recipe.strDescription : @"";
    self.recipeImageUrlField.stringValue = self.result[@"imageSrc"];

    self.window.title = recipe.strName ? recipe.strName : (self.result[@"title"] ? self.result[@"title"] : self.result[@"url"]);
}

- (NSArray<NSString *> *)control:(NSControl *)control textView:(NSTextView *)textView completions:(NSArray<NSString *> *)words forPartialWordRange:(NSRange)charRange indexOfSelectedItem:(NSInteger *)index {
    // prevent list from selecting first item
    *index = -1;

    // lookup DB and save result to instance variable
    self.autocompleteLast = [WSARecipeAutocompletionList listForString:textView.string];
    return self.autocompleteLast.stringArray;
}

// Action called when field change DB amount
- (IBAction) changedIngredientAmount:(NSTextField *)sender {
    WSRecipe *recipe = (WSRecipe *) self.result[@"recipe"];
    NSView *superview = sender.superview;
    WSIngredient *ingredient = recipe.arrayIngredients[sender.superview.tag];
    
    WSIngredientAmount *am = ingredient.arrayAmounts.count > 0 ? ingredient.arrayAmounts[0] : nil;
    if (am) {
        ingredient.arrayAmounts[0].strAmount = sender.stringValue;
        ingredient.arrayAmounts[0].nAmount = @(sender.floatValue);
    }

    // update UI amount field
    NSTextField *strAmountField = [superview viewWithTag:WSAIngredientViewUIAmount];
    strAmountField.stringValue = sender.stringValue;
}

// Action called when field change UI amount
- (IBAction)changedIngredientAmountStr:(NSTextField *)sender {
    WSRecipe *recipe = (WSRecipe *) self.result[@"recipe"];
    NSView *superview = sender.superview;
    WSIngredient *ingredient = recipe.arrayIngredients[sender.superview.tag];

    WSIngredientAmount *am = ingredient.arrayAmounts.count > 0 ? ingredient.arrayAmounts[0] : nil;
    if (am) {
        ingredient.arrayAmounts[0].strAmount = sender.stringValue;
        // in this case we need to parse stringValue first
        ingredient.arrayAmounts[0].nAmount = [WSRecipeParser parseAmount:sender.stringValue];
    }

    // update DB amount field
    NSTextField *dbAmountField = [superview viewWithTag:WSAIngredientViewDBAmount];
    dbAmountField.floatValue = [WSRecipeParser parseAmount:sender.stringValue].floatValue;
}

- (IBAction) changedIngredientAmountUnit:(NSTextField *)sender {
    WSRecipe *recipe = (WSRecipe *) self.result[@"recipe"];
    WSIngredient *ingredient = recipe.arrayIngredients[sender.superview.tag];

    WSIngredientAmount *am = ingredient.arrayAmounts.count > 0 ? ingredient.arrayAmounts[0] : nil;
    if (am) {
        ingredient.arrayAmounts[0].strUnit = sender.stringValue;
    }
}

// Method called when autocompleted field change text. It's called everytime text change occurs, whether it was user
// who changed it or autocomplete mechanism
- (void) controlTextDidChange:(NSNotification *) n {
    NSTextView *field = (NSTextView *) [[n userInfo] objectForKey:@"NSFieldEditor"];

    // since NSTextField autocompletion algorithm only works per-word we match our previously calculated autocompletion items
    // agains suffix of the string in the field. If the field contains previously created autocompletion item it's probably because
    // autocompletion put it there. Next thing we update DB entry and remove all other words from the field
    WSARecipeAutocompletionItem *selectedItem = [self.autocompleteLast itemForSuffixOf:field.string];
    if (selectedItem) {
        WSRecipe *recipe = (WSRecipe *) self.result[@"recipe"];
        
        // our real superview somewhere deep down the hierarchy
        WSIngredient *ingr = recipe.arrayIngredients[field.superview.superview.superview.tag];

        ingr.dbId = selectedItem.identifier;
        ingr.dbName = selectedItem.name;
        ingr.dbType = selectedItem.type;
        
        field.string = selectedItem.string;
    } else { // if it was not matched we start autocompletion mechanism again
        [field complete:nil];
    }
}

// This is called when any of the metadata fields change text
- (void) textDidChange:(NSNotification *)notification {
    [self changedMetadata:nil];
}

// action to update metadata
- (IBAction)changedMetadata:(id)sender {
    self.result[@"title"] = self.recipeNameField.stringValue;
    if (![self.result[@"imageSrc"] isEqualToString:self.recipeImageUrlField.stringValue]) {
        self.result[@"imageSrc"] = self.recipeImageUrlField.stringValue;
        self.recipeImageView.image = [[NSImage alloc] initWithContentsOfURL:[NSURL URLWithString:self.result[@"imageSrc"]]];
    }
    
    WSRecipe *recipe = (WSRecipe *) self.result[@"recipe"];
    recipe.strName = self.recipeNameField.stringValue;
    recipe.strYield = self.recipeYieldField.stringValue;
    recipe.strDescription = self.recipeDescriptionField.string;
}

- (IBAction)saveAction:(id)sender {
    NSLog(@"%@", self.result);
}

@end
