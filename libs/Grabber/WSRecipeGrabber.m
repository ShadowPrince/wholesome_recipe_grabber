//
//  RecipeGrabber.m
//  wholesome_recipe_grabber
//
//  Created by shdwprince on 11/18/15.
//  Copyright © 2015 shdwprince. All rights reserved.
//

#import "WSRecipeGrabber.h"

@implementation WKUserScript (UserScriptFromPath)
- (instancetype) initWithResource:(NSString *) res ofType:(NSString *) type injectionTime:(WKUserScriptInjectionTime) time {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"desktop_context_menu" ofType:@"js"];
    NSString *jsCode = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];

    return [self initWithSource:jsCode injectionTime:time forMainFrameOnly:NO];
}
@end

@interface WSRecipeGrabber ()
@property WSRecipeGrabberSource *source;
@property WSRecipeParser *parser;
@end@implementation WSRecipeGrabber

- (instancetype) initWithSource:(WSRecipeGrabberSource *)view {
    self = [super init];
    self.source = view;
    self.parser = [WSRecipeParser new];
    return self;
}

- (void) reset {
    _isCodeInjected = NO;
}

- (void) injectCode {
    [self injectJavascriptFileNamed:@"zepto.min"];
    [self injectJavascriptFileNamed:@"inject"];

    _isCodeInjected = YES;
}

#pragma mark grabber functions

- (NSString *) grabImageURLAt:(CGPoint)point {
    NSString *url =  [self callGrabberFunction:@"getImageAtXY"
                                 withArguments:@[[NSNumber numberWithFloat:point.x],
                                                 [NSNumber numberWithFloat:point.y], ]];

    if ([url hasPrefix:@"//"]) {
        url = [@"http:" stringByAppendingString:url];
    }

    if (![url hasPrefix:@"http"]) {
        url = [@"http://" stringByAppendingString:url];
    }

    return url;
}

- (WSRecipe *) grabRecipe {
    NSDictionary *dict = [self callGrabberFunction:@"getRecipe" withArguments:nil];
    return [self.parser parseRecipe:dict];
}

- (NSString *) grabDomain {
    return [self callGrabberFunction:@"getDomain" withArguments:nil];
}

- (NSString *) grabSitename {
    return [self callGrabberFunction:@"getSitename" withArguments:nil];
}

- (NSString *) grabTitle {
    return [self callGrabberFunction:@"getTitle" withArguments:nil];
}

- (NSURL *) grabURL {
    return self.source.currentUrl;
}

#pragma mark - helper

- (id) callGrabberFunction:(NSString *) fn withArguments:(NSArray *) arguments {
    NSError *e;
    NSString *serializedArguments;

    if (arguments == nil) {
        serializedArguments = @"[]";
    } else {
        NSData *serializedArgumentsData = [NSJSONSerialization dataWithJSONObject:arguments
                                                                      options:0
                                                                        error:&e];
        if (e) { // it's useless to call function without arguments
            [NSException exceptionWithName:@"callFunction error" reason:@"can't serialize arguments" userInfo:nil];
        } else {
            serializedArguments = [[NSString alloc] initWithData:serializedArgumentsData encoding:NSUTF8StringEncoding];
        }
    }

    NSMutableString *js = [NSMutableString stringWithFormat:@"__recipe_grabber.%@(\"%@\")",
                           fn,
                           serializedArguments];

    NSString *resultString = [self.source evaluateString:js];
    if (resultString && ![resultString isEqualToString:@"undefined"]) {
        id result = [NSJSONSerialization JSONObjectWithData:[resultString dataUsingEncoding:NSUTF8StringEncoding]
                                                     options:NSJSONReadingAllowFragments
                                                       error:&e];
        if (e) {
            NSLog(@"Error while evaluating JS code: %@", e);
        }

        return result;
    } else {
        return nil;
    }
}

- (void) injectJavascriptFileNamed:(NSString *) resourcename {
    NSString *path = [[NSBundle mainBundle] pathForResource:resourcename ofType:@"js"];
    NSString *jsCode = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    [self.source evaluateString:jsCode];
}

@end
