//
//  WSIngredientDatabase.m
//  wholesome_recipe_grabber
//
//  Created by shdwprince on 11/19/15.
//  Copyright © 2015 shdwprince. All rights reserved.
//

#import "WSIngredientDatabase.h"

#define INDEX_HEADER_VERSION @2
#define INDEX_NAME @"wholesome.index"

NSSet *WSDatabaseStopWords() {
    static NSSet *stopWords;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        stopWords = [NSSet setWithObjects:@"a's", @"able", @"about", @"above", @"according", @"accordingly", @"across", @"actually", @"after", @"afterwards", @"again", @"against", @"ain't", @"all", @"allow", @"allows", @"almost", @"alone", @"along", @"already", @"also", @"although", @"always", @"am", @"among", @"amongst", @"an", @"and", @"another", @"any", @"anybody", @"anyhow", @"anyone", @"anything", @"anyway", @"anyways", @"anywhere", @"apart", @"appear", @"appreciate", @"appropriate", @"are", @"aren't", @"around", @"as", @"aside", @"ask", @"asking", @"associated", @"at", @"available", @"away", @"awfully", @"be", @"became", @"because", @"become", @"becomes", @"becoming", @"been", @"before", @"beforehand", @"behind", @"being", @"believe", @"below", @"beside", @"besides", @"best", @"better", @"between", @"beyond", @"both", @"brief", @"but", @"by", @"c'mon", @"c's", @"came", @"can", @"can't", @"cannot", @"cant", @"cause", @"causes", @"certain", @"certainly", @"changes", @"clearly", @"co", @"com", @"come", @"comes", @"concerning", @"consequently", @"consider", @"considering", @"contain", @"containing", @"contains", @"corresponding", @"could", @"couldn't", @"course", @"currently", @"definitely", @"described", @"despite", @"did", @"didn't", @"different", @"do", @"does", @"doesn't", @"doing", @"don't", @"done", @"down", @"downwards", @"during", @"each", @"edu", @"eg", @"eight", @"either", @"else", @"elsewhere", @"enough", @"entirely", @"especially", @"et", @"etc", @"even", @"ever", @"every", @"everybody", @"everyone", @"everything", @"everywhere", @"ex", @"exactly", @"example", @"except", @"far", @"few", @"fifth", @"first", @"five", @"followed", @"following", @"follows", @"for", @"former", @"formerly", @"forth", @"four", @"from", @"further", @"furthermore", @"get", @"gets", @"getting", @"given", @"gives", @"go", @"goes", @"going", @"gone", @"got", @"gotten", @"greetings", @"had", @"hadn't", @"happens", @"hardly", @"has", @"hasn't", @"have", @"haven't", @"having", @"he", @"he's", @"hello", @"help", @"hence", @"her", @"here", @"here's", @"hereafter", @"hereby", @"herein", @"hereupon", @"hers", @"herself", @"hi", @"him", @"himself", @"his", @"hither", @"hopefully", @"how", @"howbeit", @"however", @"i'd", @"i'll", @"i'm", @"i've", @"ie", @"if", @"ignored", @"immediate", @"in", @"inasmuch", @"inc", @"indeed", @"indicate", @"indicated", @"indicates", @"inner", @"insofar", @"instead", @"into", @"inward", @"is", @"isn't", @"it", @"it'd", @"it'll", @"it's", @"its", @"itself", @"just", @"keep", @"keeps", @"kept", @"know", @"known", @"knows", @"last", @"lately", @"later", @"latter", @"latterly", @"least", @"less", @"lest", @"let", @"let's", @"like", @"liked", @"likely", @"little", @"look", @"looking", @"looks", @"ltd", @"mainly", @"many", @"may", @"maybe", @"me", @"mean", @"meanwhile", @"merely", @"might", @"more", @"moreover", @"most", @"mostly", @"much", @"must", @"my", @"myself", @"name", @"namely", @"nd", @"near", @"nearly", @"necessary", @"need", @"needs", @"neither", @"never", @"nevertheless", @"new", @"next", @"nine", @"no", @"nobody", @"non", @"none", @"noone", @"nor", @"normally", @"not", @"nothing", @"novel", @"now", @"nowhere", @"obviously", @"of", @"off", @"often", @"oh", @"ok", @"okay", @"old", @"on", @"once", @"one", @"ones", @"only", @"onto", @"or", @"other", @"others", @"otherwise", @"ought", @"our", @"ours", @"ourselves", @"out", @"outside", @"over", @"overall", @"own", @"particular", @"particularly", @"per", @"perhaps", @"placed", @"please", @"plus", @"possible", @"presumably", @"probably", @"provides", @"que", @"quite", @"qv", @"rather", @"rd", @"re", @"really", @"reasonably", @"regarding", @"regardless", @"regards", @"relatively", @"respectively", @"right", @"said", @"same", @"saw", @"say", @"saying", @"says", @"second", @"secondly", @"see", @"seeing", @"seem", @"seemed", @"seeming", @"seems", @"seen", @"self", @"selves", @"sensible", @"sent", @"serious", @"seriously", @"seven", @"several", @"shall", @"she", @"should", @"shouldn't", @"since", @"six", @"so", @"some", @"somebody", @"somehow", @"someone", @"something", @"sometime", @"sometimes", @"somewhat", @"somewhere", @"soon", @"sorry", @"specified", @"specify", @"specifying", @"still", @"sub", @"such", @"sup", @"sure", @"t's", @"take", @"taken", @"tell", @"tends", @"th", @"than", @"thank", @"thanks", @"thanx", @"that", @"that's", @"thats", @"the", @"their", @"theirs", @"them", @"themselves", @"then", @"thence", @"there", @"there's", @"thereafter", @"thereby", @"therefore", @"therein", @"theres", @"thereupon", @"these", @"they", @"they'd", @"they'll", @"they're", @"they've", @"think", @"third", @"this", @"thorough", @"thoroughly", @"those", @"though", @"three", @"through", @"throughout", @"thru", @"thus", @"to", @"together", @"too", @"took", @"toward", @"towards", @"tried", @"tries", @"truly", @"try", @"trying", @"twice", @"two", @"un", @"under", @"unfortunately", @"unless", @"unlikely", @"until", @"unto", @"up", @"upon", @"us", @"use", @"used", @"useful", @"uses", @"using", @"usually", @"value", @"various", @"very", @"via", @"viz", @"vs", @"want", @"wants", @"was", @"wasn't", @"way", @"we", @"we'd", @"we'll", @"we're", @"we've", @"welcome", @"well", @"went", @"were", @"weren't", @"what", @"what's", @"whatever", @"when", @"whence", @"whenever", @"where", @"where's", @"whereafter", @"whereas", @"whereby", @"wherein", @"whereupon", @"wherever", @"whether", @"which", @"while", @"whither", @"who", @"who's", @"whoever", @"whole", @"whom", @"whose", @"why", @"will", @"willing", @"wish", @"with", @"within", @"without", @"won't", @"wonder", @"would", @"wouldn't", @"yes", @"yet", @"you", @"you'd", @"you'll", @"you're", @"you've", @"your", @"yours", @"yourself", @"yourselves", @"zero", @"", @",", nil];
    });
    return stopWords;
}

@implementation WSIngredientDatabaseLookupResult @end

@interface WSIngredientDatabase ()
@property WSDatabaseWrapper *wrapper;
@property MHTextIndex *index;
@end@implementation WSIngredientDatabase

+ (instancetype) db {
    static WSIngredientDatabase *db = nil;
    if (!db)
        db = [WSIngredientDatabase new];

    return db;
}

- (instancetype) init {
    self = [super init];
    self.wrapper = [WSDatabaseWrapper getWSDataBaseWrapper];

    [self initIndex];
    return self;
}

- (void) initIndex {
    self.index = [MHTextIndex textIndexInLibraryWithName:INDEX_NAME];

    MHSearchResultItem *header = [self.index searchResultForKeyword:@"__index_header" options:0].firstObject;
    if (header && ![header.context[@"UsdaID"] isEqualToNumber:INDEX_HEADER_VERSION]) {
        [self.index deleteFromDisk];
        self.index = [MHTextIndex textIndexInLibraryWithName:INDEX_NAME];
        header = nil;
    }

    [self.index setIdentifier:^NSData *(NSDictionary *object){
        return [[NSString stringWithFormat:@"%@", object] dataUsingEncoding:NSUTF8StringEncoding];
    }];

    [self.index setIndexer:^MHIndexedObject *(NSDictionary *object, NSData *identifier){
        MHIndexedObject *o = [MHIndexedObject new];
        if (object[@"Type"]) {
            o.strings = @[object[@"Name"], object[@"Type"], ];
        } else {
            o.strings = @[object[@"Name"], ];
        }

        o.weight = 1000 - [(NSString *) object[@"Name"] length];
        o.context = @{@"Name": object[@"Name"],
                      @"UsdaID": object[@"UsdaID"],
                      @"Type": !object[@"Type"] ? @"" : object[@"Type"], };
        return o;
    }];

    if (!header) {
        [self.index indexObject:@{@"Name": @"__index_header", @"UsdaID": INDEX_HEADER_VERSION}];

        [self.wrapper prepareQuery:@"SELECT UsdaID, Name, Type from FoodTable WHERE 1 ORDER BY length(Name)"];
        NSDictionary *row;
        while ((row = [self.wrapper getPreparedRow])) {
            [self.index indexObject:row];
        }
    }
}

- (BOOL) lookupAndPopulateIngredient:(WSIngredient *) ingr {
    NSNumber *usdaID = [self lookupIngredientIdFor:ingr.strName];
    NSDictionary *row = [self fetchIngredientRowForId:usdaID];
    
    if (row) {
        ingr.dbName = row[@"Name"];
        ingr.dbType = row[@"Type"];
        ingr.dbId = [NSNumber numberWithLongLong:[(NSString *) row[@"UsdaID"] longLongValue]];
    }

    return (bool) row;
}

- (WSIngredientDatabaseLookupResult *) lookupIngredientsFor:(NSString *) term {
    // @TODO: prevent it in UI
    [self.index.indexingQueue waitUntilAllOperationsAreFinished];

    NSMutableDictionary<NSData *, MHSearchResultItem *> *resultItems = [NSMutableDictionary new];
    NSMutableDictionary<NSData *, NSNumber *> *results = [NSMutableDictionary new];

    NSArray<NSString *> *words = [term componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" \n,:()"]];

    // handle pluralization by adding unpluralized words
    NSArray<NSString *> *pluralizationSuffixes = @[@"es", @"s", ];
    NSMutableArray<NSString *> *pluralizedWords = [NSMutableArray new];
    for (NSString *word in words) {
        // skip empty and stopwords
        if ([WSDatabaseStopWords() containsObject:word])
            continue;

        if (word.length < 3)
            continue;

        NSString *suffix = nil;
        for (NSString *plurSuff in pluralizationSuffixes) {
            if ([word hasSuffix:plurSuff]) {
                suffix = plurSuff;

                [pluralizedWords addObject:[word substringToIndex:word.length-suffix.length]];
            } else {
                // if we don't do that, pluralized words will always have 1 more match
                [pluralizedWords addObject:word];
            }
        }

        [pluralizedWords addObject:word];
    }

    for (long wi = pluralizedWords.count - 1; wi >= 0; wi--) {
        NSString *word = [pluralizedWords[wi] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSMutableDictionary<NSData *, NSNumber *> *iterationResults = [NSMutableDictionary new];

        __block int i = 0;
        [self.index enumerateResultForKeyword:word
                                      options:0
                                    withBlock:^(MHSearchResultItem *resultItem, NSUInteger rank, NSUInteger count, BOOL *stop) {
                                        NSData *uid = resultItem.context[@"UsdaID"];

                                        BOOL found_in_name = NO, found_in_type = NO;
                                        for (NSIndexPath *token in resultItem.resultTokens) {
                                            switch (token.mh_string) {
                                                case 0:
                                                    found_in_name = YES;
                                                    break;
                                                case 1:
                                                    found_in_type = YES;
                                                    break;
                                            }
                                        }

                                        float weight = found_in_name * 1.f + found_in_type * 0.25f + (float) wi / 100 + (float) (count - i) / 1000;
                                        /**
                                         * weight is calculated with 3 values:
                                         * 1 - number of occurences in name
                                         * 2 - number of occurences in type
                                         * 3 - reversed position in search term
                                         * 4 - reversed length of match
                                         * the more the better
                                         */

                                        // here for debugging purposes
                                        //NSLog(@"search term: %@, result %@ %@ %@ = %f (%d, %d)", word, uid, resultItem.context[@"Type"], resultItem.context[@"Name"], weight, found_in_name, found_in_type);

                                        if (!results[uid]) {
                                            resultItems[uid] = resultItem;
                                        } else if (results[uid].floatValue < weight) {
                                            resultItems[uid] = resultItem;
                                        }

                                        iterationResults[uid] = [NSNumber numberWithFloat:weight];
                                        i++;
                                    }];

        for (NSData *key in iterationResults) {
            CGFloat weight = iterationResults[key].floatValue;

            if (results[key]) {
                weight += results[key].floatValue;
            }

            results[key] = [NSNumber numberWithFloat:weight];
        }
    }

    WSIngredientDatabaseLookupResult *lookupResult = [WSIngredientDatabaseLookupResult new];
    lookupResult.results = results;
    lookupResult.resultItems = resultItems;

    return lookupResult;
}

- (NSNumber *) lookupIngredientIdFor:(NSString *) term {
    WSIngredientDatabaseLookupResult *lookup = [self lookupIngredientsFor:term];
    // select maxItem based on item's weights
    __block float maxWeight = 0;
    __block MHSearchResultItem *maxItem = nil;
    [lookup.results enumerateKeysAndObjectsUsingBlock:^(NSData * key, NSNumber *weight, BOOL * _Nonnull stop) {
        if (weight.floatValue > maxWeight) {
            maxWeight = weight.floatValue;
            maxItem = lookup.resultItems[key];
        }
        
        // here for debugging purposes
        //MHSearchResultItem *item = (MHSearchResultItem *) lookup.resultItems[key];
        //NSLog(@"\t%f: (%@) %@, %@", weight.floatValue, item.context[@"UsdaID"], item.context[@"Type"], item.context[@"Name"]);
    }];

    // here for debugging purposes
    //NSLog(@"\n-------\n winner: %f (%@) %@ %@", maxWeight, maxItem.context[@"UsdaID"], maxItem.context[@"Type"], maxItem.context[@"Name"]);

    return maxItem.context[@"UsdaID"];
}

- (NSDictionary *) fetchIngredientRowForId:(NSNumber *) uid {
    [self.wrapper prepareQuery:@"SELECT UsdaID, name FROM FoodTable WHERE UsdaID = ? LIMIT 1", uid];
    return [self.wrapper getPreparedRow];
}

@end
