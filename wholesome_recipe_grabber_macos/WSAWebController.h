//
//  WSAWebController.h
//  wholesome_recipe_grabber
//
//  Created by shdwprince on 1/6/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>

#import "WSRecipeGrabber.h"

typedef NSMutableDictionary WSAGrabResult;

@class WSAWebController;

/**
 * Delegate interface to WSAWebController
 */
@protocol WSAWebControllerDelegate <NSObject>
- (void) webController:(WSAWebController *) instance changedURL:(NSURL *) url;
- (void) webControllerIsReady:(WSAWebController *) instance;
- (void) webController:(WSAWebController *) instance canGoBack:(BOOL) goBack forward:(BOOL) forward;
- (void) webController:(WSAWebController *) instance grabbed:(WSAGrabResult *) result;

@end

/**
 * Controller dealing with WKWebView ins and outs.
 */
@interface WSAWebController : NSObject<WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler>

// since WKWebView doesn't support IB we use this method to add WKWebView to the view
+ (WSAWebController *) instanceIntoView:(NSView *) view delegate:(NSObject<WSAWebControllerDelegate> *) delegate;

- (void) navigateBack;
- (void) navigateForward;

- (void) loadURL:(NSURL *) url;
- (void) loadURLString:(NSString *) string;

@end
