//
//  WSARecipeAutocompletion.h
//  wholesome_recipe_grabber
//
//  Created by shdwprince on 1/11/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import "WSRecipeParser.h"

// NSNib extension to reduce boilerplate code
@interface NSNib (InstantiateAndGetView)
- (NSView *) instantiateViewWithOwner:(id) owner;

@end

// Normally NSView only allow to change it's tag inside IB, but we use the tag to determine ingredient id, so here's wrapper to make it readwrite
@interface WSAView : NSView
@property (readwrite) NSInteger tag;

@end

/**
 * Autocompletion item. Since we can identify selected item only by it's contents we need this to store DB info.
 */
@interface WSARecipeAutocompletionItem : NSObject
@property NSNumber *identifier;
@property NSString *type;
@property NSString *name;
@property NSString *string;

// Item from data
+ (WSARecipeAutocompletionItem *) itemWithIdentifier:(NSNumber *) identifier name:(NSString *) name type:(NSString *) type;
// String from ingredient
+ (NSString *) formattedIngredient:(WSIngredient *) ingredient;
@end

/**
 * Basically a wrapper around NSArray providing some extra methods.
 */
@interface WSARecipeAutocompletionList : NSObject
// Create autocompletion list from given input string
+ (WSARecipeAutocompletionList *) listForString:(NSString *) searchString;

- (NSArray<NSString *> *) stringArray;
// Get autocompletion item stored in the suffix of the string
- (WSARecipeAutocompletionItem *) itemForSuffixOf:(NSString *) string;

@end
