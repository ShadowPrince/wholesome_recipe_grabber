//
//  main.m
//  wholesome_recipe_grabber_macos
//
//  Created by shdwprince on 1/4/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
