//
//  ResultsViewController.m
//  wholesome_recipe_grabber
//
//  Created by shdwprince on 11/18/15.
//  Copyright © 2015 shdwprince. All rights reserved.
//

#import "ResultsViewController.h"

@interface ResultsViewController ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *siteLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *imageLoadingIndicator;
@property (weak, nonatomic) IBOutlet UITextView *ingredientsTextView;
@property (weak, nonatomic) IBOutlet UILabel *yieldLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorLabel;

@end@implementation ResultsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = self.results[@"title"];
    self.siteLabel.text = self.results[@"sitename"];

    WSRecipe *r = self.results[@"recipe"];

    self.titleLabel.text = r.strName ? r.strName : @"No name";
    self.descriptionTextView.text = r.strDescription ? r.strDescription : @"No description";
    self.authorLabel.text = r.strAuthor ? r.strAuthor : @"";
    self.yieldLabel.text = r.strYield ? [@"Yield: " stringByAppendingString:r.strYield] : @"";

    NSMutableString *ingredientsString = [NSMutableString new];
    for (WSIngredient *i in r.arrayIngredients) {
        if (i.strFallback) {
            [ingredientsString appendString:i.strFallback];
        } else {
            if (i.dbId) {
                [ingredientsString appendFormat:@"#%@(%@) ", i.dbId, i.dbName];
            }

            for (WSIngredientAmount *amount in i.arrayAmounts) {
                [ingredientsString appendString:[NSString stringWithFormat:@"%@ %@",
                                                 amount.nAmount,
                                                 amount.strUnit ? amount.strUnit : @""]];
                if (amount.strNote)
                    [ingredientsString appendString:[NSString stringWithFormat:@" (%@)", amount.strNote]];
            }

            [ingredientsString appendString:[NSString stringWithFormat:@" %@", i.strName]];
            if (i.strNote)
                [ingredientsString appendString:[NSString stringWithFormat:@"(%@)", i.strNote]];
        }

        [ingredientsString appendString:@"\n\n"];
    }
    self.ingredientsTextView.text = ingredientsString;

    if (self.results[@"imageSrc"] != [NSNull null]) {
        NSString *imageSrc = self.results[@"imageSrc"];
        __weak ResultsViewController *_self = self;
        [[NSOperationQueue new] addOperationWithBlock:^{
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageSrc]];

            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [_self.imageLoadingIndicator stopAnimating];
                _self.imageView.image = [[UIImage alloc] initWithData:data];
            }];
        }];
    } else {
        [self.imageLoadingIndicator stopAnimating];
    }
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

    self.imageView.image = nil;
}

@end
