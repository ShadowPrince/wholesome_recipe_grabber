//
//  WSRecipe.m
//  wholesome_recipe_grabber
//
//  Created by shdwprince on 11/18/15.
//  Copyright © 2015 shdwprince. All rights reserved.
//

#import "WSRecipe.h"

@implementation WSIngredientAmount

- (NSString *) description {
    return [NSString stringWithFormat:@"%@ %@ %@ (%@)",
            [super description],
            self.nAmount,
            self.strUnit,
            self.strNote];
}

- (NSString *) inputString {
    if (self.strFallback) {
        return [NSString stringWithFormat:@"%@", self.strFallback];
    } else {
        return [NSString stringWithFormat:@"%@%@%@",
                self.strAmount,
                self.strUnit ? [@"" stringByAppendingString:self.strUnit] : @"",
                self.strNote ? [@" " stringByAppendingString:self.strNote] : @""];
    }
}

@end

@implementation WSIngredient

- (NSString *) description {
    return [NSString stringWithFormat:@"%@ %@ #%@ %@, %@(%@)",
            [super description],
            self.arrayAmounts,
            self.dbId,
            self.dbType,
            self.dbName,
            self.strNote];
}

- (NSString *) inputString {
    if (self.strFallback) {
        return self.strFallback;
    } else {
        NSMutableString *result = [NSMutableString new];
        for (WSIngredientAmount *am in self.arrayAmounts) {
            [result appendString:[am.inputString stringByAppendingString:@" "]];
        }

        [result appendFormat:@"%@%@", self.strName, self.strNote ? [@" " stringByAppendingString:self.strNote] : @""];
        return result;
    }
}

@end

@implementation WSRecipe

- (NSString *) description {
    return [NSString stringWithFormat:@"%@ %@, (%@), ingredients: %@",
            [super description],
            self.strName,
            self.strDescription,
            self.arrayIngredients.description];
}

@end
