//
//  WSRecipe.h
//  wholesome_recipe_grabber
//
//  Created by shdwprince on 11/18/15.
//  Copyright © 2015 shdwprince. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSRegexHelperMatch.h"

/*
 * Class, used to represent 1 amount of ingredient
 * sometimes there's multiple ones, like 1 tsp + 1 cup
 */
@interface WSIngredientAmount : NSObject
@property NSNumber *nAmount;
@property NSString *strAmount;
@property NSString *strUnit;
@property NSString *strNote;
// if parsing failed, above values will be nil and strFallback will contain original string
@property NSString *strFallback;

- (NSString *) inputString;

@end

/*
 * Class, used to represent single ingredient
 * Ingredient contain 1 or more amounts, stored in arrayAmounts
 */
@interface WSIngredient : NSObject
@property NSArray<WSIngredientAmount *> *arrayAmounts;
// str* properties is parsed string data (used for db lookup)
@property NSString *strName;
@property NSString *strNote;
// if parsing failed, above values will be nil and strFallback will contain original string
@property NSString *strFallback;
// db* properties is matched data from database
@property NSNumber *dbId;
@property NSString *dbName;
@property NSString *dbType;

- (NSString *) inputString;

@end

/*
 * Class, used to represent recipe.
 * Recipe contain 0 or more ingredients, stored in arrayIngredients
 */
@interface WSRecipe : NSObject
@property NSString *strName, *strDescription, *strYield, *strAuthor;
@property NSArray<WSIngredient *> *arrayIngredients;

@end
