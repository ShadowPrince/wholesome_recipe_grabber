//
//  AppDelegate.h
//  wholesome_recipe_grabber_macos
//
//  Created by shdwprince on 1/4/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "WSIngredientDatabase.h"
#import "WSAWebController.h"
#import "WSAWebWindowController.h"
#import "WSARecipeWindowController.h"

@class WSARecipeWindowController;

@interface AppDelegate : NSObject <NSApplicationDelegate>

- (void) presentGrabbedResult:(WSAGrabResult *) result;
- (void) removeController:(WSARecipeWindowController *) controller;

@end

