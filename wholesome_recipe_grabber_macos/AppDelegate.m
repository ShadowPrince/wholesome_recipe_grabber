//
//  AppDelegate.m
//  wholesome_recipe_grabber_macos
//
//  Created by shdwprince on 1/4/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import "AppDelegate.h"


@interface AppDelegate ()
// Used to keep recipeControllers from deallocating
@property NSMutableArray<WSARecipeWindowController *> *recipeControllers;

@end @implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    [WSIngredientDatabase db];

    self.recipeControllers = [NSMutableArray new];
}

- (void) presentGrabbedResult:(WSAGrabResult *)result {
    [self.recipeControllers addObject:[WSARecipeWindowController presentedControllerWith:result]];
}

- (void) removeController:(WSARecipeWindowController *) controller {
    [self.recipeControllers removeObject:controller];
}

@end
