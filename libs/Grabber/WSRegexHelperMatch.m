//
//  WSRegexHelper.m
//  wholesome_recipe_grabber
//
//  Created by shdwprince on 11/19/15.
//  Copyright © 2015 shdwprince. All rights reserved.
//

#import "WSRegexHelperMatch.h"

@implementation WSRegexHelperMatch

- (NSString *) description {
    return [NSString stringWithFormat:@"%@ match of %@ at %d:%d",
            [super description],
            self.match,
            self.range.location,
            self.range.length];
}

+ (NSArray*) matchSplitArrayBySplitting:(NSString *)str withRegex:(NSString *) regex {
    NSRange range = NSMakeRange(0, str.length);

    NSRegularExpression *expr = [WSRegexHelperMatch compileRegex:regex options:0 error:nil];
    //get locations of matches
    NSMutableArray* matchingRanges = [NSMutableArray array];
    NSArray* matches = [expr matchesInString:str options:0 range:range];
    for(NSTextCheckingResult* match in matches) {
        [matchingRanges addObject:[NSValue valueWithRange:match.range]];
    }

    //invert ranges - get ranges of non-matched pieces
    NSMutableArray* pieceRanges = [NSMutableArray array];
    NSMutableArray *pieceSplits = [NSMutableArray array];

    //add first range
    [pieceRanges addObject:[NSValue valueWithRange:NSMakeRange(0,
                                                               (matchingRanges.count == 0 ? str.length : [matchingRanges[0] rangeValue].location))]];

    //add between splits ranges and last range
    for(int i=0; i<matchingRanges.count; i++){
        BOOL isLast = i+1 == matchingRanges.count;
        unsigned long startLoc = [matchingRanges[i] rangeValue].location + [matchingRanges[i] rangeValue].length;
        unsigned long endLoc = isLast ? str.length : [matchingRanges[i+1] rangeValue].location;
        [pieceRanges addObject:[NSValue valueWithRange:NSMakeRange(startLoc, endLoc-startLoc)]];
        [pieceSplits addObject:[str substringWithRange:[matchingRanges[i] rangeValue]]];
    }

    //use split ranges to select pieces
    NSMutableArray* pieces = [NSMutableArray array];
    for (int i = 0; i < pieceRanges.count; i++) {
        NSValue *val = pieceRanges[i];

        NSRange range = [val rangeValue];
        NSString* piece = [str substringWithRange:range];
        [pieces addObject:@[i >= 1 ? pieceSplits[i-1] : [NSNull null], piece]];
    }
    
    return pieces;
}

+ (NSRegularExpression *) compileRegex:(NSString *)regex
                               options:(NSRegularExpressionOptions)opts
                                 error:(NSError *__autoreleasing *)error {
    return [NSRegularExpression regularExpressionWithPattern:regex
                                                     options:opts
                                                       error:error];
}

+ (NSString *) stringByReplacingRegex:(NSString *)regex
                          withOptions:(NSRegularExpressionOptions)opts
                         withTemplate:(NSString *)tpl
                             inString:(NSString *)str
                                error:(NSError *__autoreleasing *) error {
    NSRegularExpression *regularExpression = [WSRegexHelperMatch compileRegex:regex options:opts error:error];
    return [WSRegexHelperMatch stringByReplacingRegularExpression:regularExpression
                                                     withTemplate:tpl
                                                         inString:str];
}

+ (NSString *) stringByReplacingRegularExpression:(NSRegularExpression *) expr
                                     withTemplate:(NSString *) tpl
                                         inString:(NSString *) str {
    return [expr stringByReplacingMatchesInString:str
                                          options:0
                                            range:NSMakeRange(0, str.length)
                                     withTemplate:tpl];
}

+ (NSArray<WSRegexHelperMatch *> *) firstMatchOfRegex:(NSString *)regex
                                          withOptions:(NSRegularExpressionOptions)opts
                                             inString:(NSString *)str
                                                error:(NSError *__autoreleasing *)error {
    NSRegularExpression *regularExpression = [WSRegexHelperMatch compileRegex:regex options:opts error:error];
    return [WSRegexHelperMatch firstMatchOfRegularExpression:regularExpression inString:str];
}

+ (NSArray<WSRegexHelperMatch *> *) firstMatchOfRegularExpression:(NSRegularExpression *) expr
                                                         inString:(NSString *) str {
    NSTextCheckingResult *match = [expr firstMatchInString:str options:0 range:NSMakeRange(0, str.length)];
    NSMutableArray<WSRegexHelperMatch *> *matches = [NSMutableArray new];

    for (int i = 0; i < match.numberOfRanges; i++) {
        NSRange r = [match rangeAtIndex:i];
        if (r.length != 0) {
            WSRegexHelperMatch *helperMatch = [WSRegexHelperMatch new];
            helperMatch.match = [str substringWithRange:r];
            helperMatch.range = r;

            [matches addObject:helperMatch];
        }
    }

    return matches;
}

@end
