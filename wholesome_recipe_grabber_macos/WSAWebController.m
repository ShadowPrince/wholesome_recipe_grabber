//
//  WSAWebController.m
//  wholesome_recipe_grabber
//
//  Created by shdwprince on 1/6/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import "WSAWebController.h"

// WSRecipGrabberSource implementation for OSX' WKWebView
@implementation WKWebView (WSRecipeGrabberSource)
- (NSURL *) currentUrl {
    return self.URL;
}

- (NSString *) evaluateString:(NSString *) string {
    __block BOOL done = false;
    __block NSString *result = nil;
    [self evaluateJavaScript:string completionHandler:^(id _Nullable ret, NSError * _Nullable error) {
        done = true;
        result = [NSString stringWithFormat:@"%@", ret];
    }];

    // since evaluateJavaScript in WKWebView is async we wait for it here
    while (!done) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }

    return result;
}

@end

@interface WSAWebController ()
@property (weak) NSObject<WSAWebControllerDelegate> *delegate;
@property WKUserContentController *menuController;

@property WKUserScript *webViewScript;
@property WKWebView *webView;
@property NSString *lastInjectUrl;
@property WSRecipeGrabber *grabber;

@end @implementation WSAWebController

+ (WSAWebController *) instanceIntoView:(NSView *) view delegate:(NSObject<WSAWebControllerDelegate> *) delegate {
    WSAWebController *i = [WSAWebController new];
    i.delegate = delegate;

    // desktop script
    NSString *path = [[NSBundle mainBundle] pathForResource:@"desktop_context_menu" ofType:@"js"];
    NSString *jsCode = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    i.webViewScript = [[WKUserScript alloc] initWithSource:jsCode injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:NO];

    // user content controller. "contextMenuHandler" action from JS will be received in webkit's delegate (this instance)
    i.menuController = [WKUserContentController new];
    [i.menuController addScriptMessageHandler:i name:@"contextMenuHandler"];

    // config
    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    config.userContentController = i.menuController;
    // enable Inspector. Keep in mind that constraints break inspector, so you have to disable them for it to work
    // [config.preferences setValue:@YES forKey:@"developerExtrasEnabled"];

    // web view
    i.webView = [[WKWebView alloc] initWithFrame:view.bounds configuration:config];
    i.webView.translatesAutoresizingMaskIntoConstraints = NO;
    i.webView.navigationDelegate = i;
    i.webView.UIDelegate = i;

    [view addSubview:i.webView];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[x]|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:@{@"x": i.webView, }]];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[x]|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:@{@"x": i.webView, }]];

    // grabber
    i.grabber = [[WSRecipeGrabber alloc] initWithSource:(WSRecipeGrabberSource *) i.webView];

    return i;
}

#pragma mark - actions
- (void) loadURL:(NSURL *)url {
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:req];
}

- (void) loadURLString:(NSString *) string {
    if (![string hasPrefix:@"http"]) {
        string = [@"http://" stringByAppendingString:string];
    }

    [self loadURL:[NSURL URLWithString:string]];
}

- (void) navigateBack {
    [self.webView goBack];
}

- (void) navigateForward {
    [self.webView goForward];
}

#pragma mark - webkit handlers
- (void) userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    // action handler. we only need coordinates of a click to get the image
    NSArray<NSNumber *> *coordinates = message.body[@"body"];
    CGPoint loc = CGPointMake(coordinates[1].floatValue, coordinates[0].floatValue);

    NSMutableDictionary *results = [NSMutableDictionary new];

    [results addEntriesFromDictionary:@{@"domain": [self.grabber grabDomain] ? [self.grabber grabDomain] : @"",
                                        @"sitename": [self.grabber grabSitename] ? [self.grabber grabSitename] : @"",
                                        @"url": [self.grabber grabURL],
                                        @"title": [self.grabber grabTitle] ? [self.grabber grabTitle] : @"",
                                        @"imageSrc": [self.grabber grabImageURLAt:loc] ? [self.grabber grabImageURLAt:loc] : @"", }];

    results[@"recipe"] = [self.grabber grabRecipe];

    [self.delegate webController:self grabbed:results];
}

#pragma mark - webkit navigation
- (void) webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
    [self.delegate webController:self canGoBack:self.webView.canGoBack forward:self.webView.canGoForward];
}

- (void) webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    [self.delegate webController:self changedURL:self.webView.URL];
}

- (void) webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    if (![self.lastInjectUrl isEqualToString:self.webView.URL.absoluteString]) {
        [self.grabber reset];
    }

    if (!self.grabber.isCodeInjected) {
        [self.grabber injectCode];
        [self.webView evaluateString:self.webViewScript.source];
        self.lastInjectUrl = self.webView.URL.absoluteString;
    }

    [self.delegate webController:self canGoBack:webView.canGoBack forward:webView.canGoForward];
    [self.delegate webControllerIsReady:self];
}

- (WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures {

    // a way to correctly handle _blank hyperlinks in wkwebview
    if (!navigationAction.targetFrame.isMainFrame) {
        [self.webView loadRequest:navigationAction.request];
    }

    return nil;
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler {
    decisionHandler(WKNavigationResponsePolicyAllow);
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {

    if (decisionHandler) {
        decisionHandler(WKNavigationActionPolicyAllow);
    }
}

@end
