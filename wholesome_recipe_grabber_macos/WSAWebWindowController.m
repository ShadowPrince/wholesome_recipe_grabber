//
//  WSAWebWindowController.m
//  wholesome_recipe_grabber
//
//  Created by shdwprince on 1/6/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import "WSAWebWindowController.h"

@interface WSAWebWindowController ()
@property WSAWebController *webController;

@property (weak) IBOutlet NSView *webPlaceholderView;

@property (weak) IBOutlet NSButton *toolbarBackButton;
@property (weak) IBOutlet NSButton *toolbarForwardButton;
@property (weak) IBOutlet NSTextField *toolbarUrlField;
@property (weak) IBOutlet NSProgressIndicator *toolbarIndicator;

@end @implementation WSAWebWindowController

- (void) awakeFromNib {
    self.webController = [WSAWebController instanceIntoView:self.webPlaceholderView delegate:self];
}

#pragma mark - actions
- (IBAction)backAction:(id)sender {
    [self.webController navigateBack];
}

- (IBAction)forwardAction:(id)sender {
    [self.webController navigateForward];
}

- (IBAction)clipboardGoAction:(id)sender {
    NSString *url = [[NSPasteboard generalPasteboard] stringForType:NSPasteboardTypeString];
    if (url) {
        [self.webController loadURLString:url];
    }
}

- (IBAction)goAction:(id)sender {
    [self.webController loadURLString:self.toolbarUrlField.stringValue];
}

#pragma mark - web controller
- (void) webController:(WSAWebController *)instance canGoBack:(BOOL)goBack forward:(BOOL)forward {
    [self.toolbarBackButton setEnabled:goBack];
    [self.toolbarForwardButton setEnabled:forward];
}

- (void) webController:(WSAWebController *)instance changedURL:(NSURL *)url {
    self.toolbarUrlField.stringValue = url.absoluteString;
    [self.toolbarIndicator startAnimation:nil];
}

- (void) webControllerIsReady:(WSAWebController *)instance {
    [self.toolbarIndicator stopAnimation:nil];
}

- (void) webController:(WSAWebController *)instance grabbed:(WSAGrabResult *)result {
    [(AppDelegate *) [NSApp delegate] presentGrabbedResult:result];
}

@end
