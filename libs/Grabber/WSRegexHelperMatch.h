//
//  WSRegexHelper.h
//  wholesome_recipe_grabber
//
//  Created by shdwprince on 11/19/15.
//  Copyright © 2015 shdwprince. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WSRegexHelperMatch;

typedef NSArray<WSRegexHelperMatch *> WSRegexHelperMatches;

@interface WSRegexHelperMatch : NSObject
@property NSRange range;
@property NSString *match;

+ (NSRegularExpression *) compileRegex:(NSString *) regex
                               options:(NSRegularExpressionOptions) opts
                                 error:(NSError *__autoreleasing *) error;

+ (NSString *) stringByReplacingRegex:(NSString *) regex
                          withOptions:(NSRegularExpressionOptions) opts
                         withTemplate:(NSString *) tpl
                             inString:(NSString *) str
                                error:(NSError *__autoreleasing *) error ;

+ (NSArray<WSRegexHelperMatch *> *) firstMatchOfRegex:(NSString *) regex
                                          withOptions:(NSRegularExpressionOptions) opts
                                             inString:(NSString *) str
                                                error:(NSError *__autoreleasing *) error ;

+ (NSArray<WSRegexHelperMatch *> *) firstMatchOfRegularExpression:(NSRegularExpression *) expr
                                                         inString:(NSString *) str;

+ (NSString *) stringByReplacingRegularExpression:(NSRegularExpression *) expr
                                     withTemplate:(NSString *) tpl
                                         inString:(NSString *) str;

// returns array of [match, part] arrays
+ (NSArray*) matchSplitArrayBySplitting:(NSString *)str withRegex:(NSString *) regex;

@end
