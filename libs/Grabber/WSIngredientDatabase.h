//
//  WSIngredientDatabase.h
//  wholesome_recipe_grabber
//
//  Created by shdwprince on 11/19/15.
//  Copyright © 2015 shdwprince. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "WSDatabaseWrapper.h"

#if TARGET_OS_IPHONE
    #import <UIKit/UIKit.h>
#elif TARGET_OS_MAC
    #import <AppKit/AppKit.h>
#endif

#import "MHTextSearch/MHTextSearch.h"

#import "WSRecipe.h"

@interface WSIngredientDatabaseLookupResult : NSObject
@property NSDictionary *results;
@property NSDictionary *resultItems;

@end

@interface WSIngredientDatabase : NSObject
+ (instancetype) db;

- (NSNumber *) lookupIngredientIdFor:(NSString *) term;
- (BOOL) lookupAndPopulateIngredient:(WSIngredient *) ingr;
- (WSIngredientDatabaseLookupResult *) lookupIngredientsFor:(NSString *) term;

@end
