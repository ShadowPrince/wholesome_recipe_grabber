//
//  wholesome_recipe_databaseTests.m
//  wholesome_recipe_grabber
//
//  Created by shdwprince on 11/19/15.
//  Copyright © 2015 shdwprince. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "WSDatabaseWrapper.h"
#import "WSRecipeParser.h"

@interface wholesome_recipe_databaseTests : XCTestCase
@property WSRecipeParser *parser;
@property WSIngredientDatabase *db;
@end

@implementation wholesome_recipe_databaseTests

- (void)setUp {
    [super setUp];

    self.parser = [WSRecipeParser new];
    self.db = [WSIngredientDatabase db];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)databaseLookupTestFor:(NSArray *) testStrings
              expectedResults:(NSArray *) testResults {
    for (int i = 0; i < testStrings.count; i++) {
        NSArray<WSIngredient *> *ingredients = [self.parser parseIngredients:testStrings[i]];
        NSObject *testResult = testResults[i];

        for (WSIngredient *ingred in ingredients) {
            [self.db lookupAndPopulateIngredient:ingred];

            if (testResult == [NSNull null]) {
                XCTAssertEqualObjects(ingred.dbName, nil);
            } else if ([testResult isKindOfClass:[NSString class]]){
                XCTAssertEqualObjects(ingred.dbName, testResult);
            } else if ([testResult isKindOfClass:[NSNumber class]]) {
                XCTAssertEqualObjects(ingred.dbId, testResult);
            } else {
                XCTAssertEqualObjects(ingred, testResult);
            }
        }
    }
}

- (void)testSuccess {
    NSArray<NSString *> *testStrings = @[@"1¼ cup dark chocolate chips",
                                         @"6Tbsp vegetable/ olive/ coconut",
                                         @"1 cup (160 g) coconut oil, scoopable (not liquid)* (or, use this crust instead)",
                                         @"1/4 cup (55 g) dark or light muscovado sugar (or sub organic brown sugar or coconut sugar)",
                                         @"½ tsp ground cinnamon (optional)",
                                         @"10kg (350o) chicken meat",
                                         @"10kg small eggs",
                                         @"1kg large egg",
                                         @"1 chicken egg",
                                         @"1 acorn squash",
                                         @"1kg sweet potato",
                                         ];
    NSArray<NSObject *> *testResults = @[@"dark chocolate",
                                         @"coconut",
                                         @"coconut oil",
                                         @"sugar",
                                         @"cinnamon",
                                         @"chicken",
                                         @"egg",
                                         @"egg",
                                         @"egg",
                                         @"acorn squash",
                                         @"sweet potato",
                                         ];

    [self databaseLookupTestFor:testStrings expectedResults:testResults];
}

- (void) testProblematic {
    [self databaseLookupTestFor:@[@"1 egg of the chicken",
                                  @"¼ cup oat flour (finely ground from rolled oats)",
                                  ]
                expectedResults:@[@"chicken", // should be "egg"
                                  @"wheat flour", // there's no oat flour in the db, but there's the others
                                  ]];
}

- (void) testFulltextSpecific {
    [self databaseLookupTestFor:@[
                                  @"1 tsp blah blah dark very very foobar chocolate i string",
                                  @"1 piece milk very chocolate",
                                  @"1 bag of wheat fresh flour",
                                  @"1 cup banana fresh chips",
                                  ]
                expectedResults:@[@"dark chocolate",
                                  @"milk chocolate",
                                  @"wheat flour",
                                  @"banana chips",
                                  ]];
}

- (void) testInvalidIngredients {
    [self databaseLookupTestFor:@[@"Toppings (optional)",
                                  @"foobar"]
                expectedResults:@[[NSNull null],
                                  [NSNull null], 
                                  ]];
}

- (void) testType {
    [self databaseLookupTestFor:@[@"1 kg turkey meat",
                                  @"1 kg chicken breast meat",
                                  @"1 piece milk very chocolate",
                                  @"1 bag of wheat fresh flour",
                                  @"1 cup raw banana fresh chips", ]
                expectedResults:@[@5168,
                                  @5064,
                                  @19120,
                                  @20581,
                                  @19400,
                                  ]];
}

- (void) testPluralization {
    [self databaseLookupTestFor:@[@"1 kg sweet potato",
                                  @"1 kg sweet potatoes",
                                  @"1 milk chocolate",
                                  @"1 chicken meats", ]
                expectedResults:@[@"sweet potato",
                                  @"sweet potato",
                                  @"milk chocolate",
                                  @"chicken",
                                  ]];
}

- (void) testWithRecipeParser {
    WSRecipe *r = [self.parser parseRecipe:@{@"ingredients": @[@"1 cup (160 g) coconut oil, scoopable (not liquid)* (or, use this crust instead)",
                                                               @"1/4 cup (55 g) dark or light muscovado sugar (or sub organic brown sugar or coconut sugar)",
                                                               @"10kg (350o) chicken meat",
                                                               @"10kg small eggs",
                                                               @"1kg large egg",
                                                               @"1 chicken egg",],
                                             @"name": @"foobar",
                                             }];
    NSArray *expectedResults = @[@"coconut oil",
                                 @"sugar",
                                 @"chicken",
                                 @"egg",
                                 @"egg",
                                 @"egg",];

    NSMutableArray *results = [NSMutableArray new];
    for (WSIngredient *i in r.arrayIngredients) {
        [results addObject:i.dbName ? i.dbName : [NSNull null]];
    }

    XCTAssertEqualObjects(results, expectedResults);
}

@end
