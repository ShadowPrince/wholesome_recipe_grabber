/*
 * This is main injected JS script.
 * It adds global __recipe_grabber variable, which contains of all functions that will be called from objective-c. 
 * All the communications is done in JSON (arguments and returned values).
 *
 * NOTE: this file should be injected AFTER zepto.min.js.
 * NOTE: __recipe_grabber_$ - renamed Zepto function to avoid conflicts
 * NOTE: you can debug this file by connecting safari to simulator webview trough developer menu
 */

var __recipe_grabber = {
    // method called on document.ready
    ready: function () {
        document.body.style.webkitTouchCallout='none';
        document.body.style.KhtmlUserSelect='none';
        document.documentElement.style.webkitUserSelect='none';
        document.documentElement.style.webkitTouchCallout='none';
    },

    // helper method used to call methods registered by register_function
    // sample usage: __recipe_grabber.call("getDomain")
    call: function() {
        var args_array = Array.prototype.slice.call(arguments);
        return JSON.parse(__recipe_grabber[args_array[0]](JSON.stringify(args_array.slice(1))));
    },

    // register function for using with RecipeGrabber
    // resulting function will be stored in __recipe_grabber and receive/return arguments as JSON
    // body still operate as usual
    register_function: function(name, body) {
        __recipe_grabber[name] = function (json_arguments) {
            var result = body.apply(body, JSON.parse(json_arguments));
            return JSON.stringify(result);
        };
    },

    // lookup elements inside Recipe itemscope
    _itemscope_query: function(sel) {
        return __recipe_grabber_$("*[itemtype='http://schema.org/Recipe'][itemscope] " + sel);
    },

    // get values from itemprop's inside Recipe itemscope
    // function return null (if none found), string (if one found) or array of strings (if multiple)
    _itemscope_getvalue: function(sel) {
        var result = [];
        __recipe_grabber._itemscope_query("*[itemprop='"+sel+"']").each(function (idx) {
            if (this.textContent == null)
                return;

            var text = this.textContent.trim();
            if (text)
                result.push(text);
        });

        switch (result.length) {
            case 0: return null;
            case 1: return result[0];
            default: return result;
        }
    }
};

// returns document.title (String)
__recipe_grabber.register_function("getTitle", function () {
    return document.title;
});

// returns document.domain (String)
__recipe_grabber.register_function("getDomain", function () {
    return document.domain;
});

// returns og:site_name OR og:title OR domain
__recipe_grabber.register_function("getSitename", function () {
    var $meta_name = __recipe_grabber_$("meta[property=og\\:site_name]");
    var $meta_title = __recipe_grabber_$("meta[property=og\\:title]");

    if ($meta_name.length) {
        return $meta_name.attr("content");
    } else if ($meta_title.length) {
        return $meta_title.attr("content");
    } else {
        return __recipe_grabber.call("getDomain");
    }
});

// get recipe dictionary. keys are predefined, values may be null
__recipe_grabber.register_function("getRecipe", function () {
    // list of itemprop names to grab
    var keys = [
        "ingredients",
        "recipeIngredients", "recipeInstructions", "recipeYield",
        "name", "description", "image", "author",

    ];
    var result = {};
    var value_transformers = {
        // could be used to parse integers and such
    };

    var transform_value = function (key, val) {
        return value_transformers[key] ? value_transformers[key](val): val;
    }

    for (idx in keys) {
        var key = keys[idx];

        var value = __recipe_grabber._itemscope_getvalue(key);

        if (value) {
            // pass value trough transformers
            if (Array.isArray(value)) {
                for (value_idx in value) {
                    value[value_idx] = transform_value(key, value[value_idx]);
                }
            } else {
                value = transform_value(key, value);
            }
        }

        result[key] = value;
    }

    return result;
});

// returns url of the image (String) or null
__recipe_grabber.register_function("getImageAtXY", function (x, y) {
    var result = null;

    var $el = __recipe_grabber_$(document.elementFromPoint(x, y));
    var $image_childs = $el.find("img");
    // if element at XY is already an image - add it to the list
    if ($el.prop("tagName") == "IMG")
        $image_childs = $image_childs.add($el);

    var max_size = 0;

    $image_childs.each(function (idx) {
        // cycle trough list of IMGs, selecting the biggest
        var $this = __recipe_grabber_$(this);
        var size = $this.width() * $this.height();
        if (size > max_size) {
            max_size = size;
            if ($this.attr("src")) {
                result = $this.attr("src");
            } else {
                var srcset = $this.attr("srcset");
                result = srcset.split(", ")[0]
            }
        }
    });

    if (!result) {
        // if there's no IMG present at XY
        // cycle trough all of child elements
        // searching for the one with background-url attribute
        var $iter = $el.parents();
        $iter = $iter.add($el);
        $iter = $iter.add($el.find('*'));

        $iter.each(function (idx) {
            var $this = __recipe_grabber_$(this);

            if ($this.css('background-image')) {
                var url = $this.css('background-image');
                result = url.substr(4, url.length - 5);
                if (result)
                    return false;
            }
        });
    }

    return result;
});

__recipe_grabber_$(document).ready(function () {
    // remove this line after testing
    alert("inject.js successfully injected");
    __recipe_grabber.ready();
});

