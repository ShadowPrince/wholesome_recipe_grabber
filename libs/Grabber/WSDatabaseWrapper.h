//
//  WSDatabaseWrapper.h
//  Wholesome
//
//  Created by Sachin Hegde on 1/9/14.
//  Copyright (c) 2015 Wholesome, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

#define RECIPEVERSION 1


@interface WSDatabaseWrapper : NSObject <NSFastEnumeration> {
	sqlite3 *database;
    sqlite3_stmt *statement;
    NSString *tableName;
    NSString *databaseFileName;
    NSFileManager *filemanager;
    
    // for "fast enumeration" (iterator/generator pattern)
    __unsafe_unretained NSDictionary * enumRows[1]; // enumerated (iterator) object(s) are passed in a C array
    NSDictionary *test;
    // we only ever pass one at a time
}

+ (WSDatabaseWrapper *) getWSDataBaseWrapper;

// object management
- (WSDatabaseWrapper *) initWholesomeDB;
- (WSDatabaseWrapper *) initWithDBFilename: (NSString *) fn;
- (WSDatabaseWrapper *) initWithDBFilename: (NSString *) fn andTableName: (NSString *) tn;
- (void) openDB;
- (void) closeDB;
- (void) dealloc;
- (NSString *) getDBPath;

// Fast enumeration (iteration) support
- (NSUInteger) countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(id __unsafe_unretained *)stackbuf count:(NSUInteger)len;

// SQL queries
- (NSNumber *) doQuery:(NSString *) query, ...;
- (WSDatabaseWrapper *) getQuery:(NSString *) query, ...;
- (void) prepareQuery:(NSString *) query, ...;
- (id) valueFromQuery:(NSString *) query, ...;

// Raw results
- (void) bindSQLWithRecord:(const char *) cQuery withVargs:(NSDictionary *)record;
- (void) bindSQL:(const char *) cQuery withVargs:(va_list)vargs;
- (NSMutableDictionary *) getPreparedRow;
- (id) getPreparedValue;

// CRUD methods
- (NSNumber *) insertRow:(NSDictionary *) record;
- (void) updateRow:(NSDictionary *) record forRowID: (NSNumber *) rowID;
- (void) deleteRow:(NSNumber *) rowID;
- (NSDictionary *) getRow: (NSNumber *) rowID;
- (NSNumber *) countRows;

// Subscripting methods
- (NSDictionary *) objectForKeyedSubscript: (NSNumber *) rowID;
- (void) setObject:(NSDictionary *) record forKeyedSubscript: (NSNumber *) rowID;

// Utilities
- (id) columnValue:(int) columnIndex;
- (NSNumber *) lastInsertId;


@end
